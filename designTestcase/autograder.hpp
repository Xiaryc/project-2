#include <iostream>
#include <iomanip>
#define LINE_LENGTH 4
#define TEST_LIMIT 5

bool isSame(int [], int []);
bool testFunc(void (*)(int*), int [][LINE_LENGTH]);

class Autograder
{
public:

    Autograder();
    ~Autograder();

    void runAutograder();
    void test( int testcase[][LINE_LENGTH] );

private:
    int m_counter;
    bool m_status[100];

};